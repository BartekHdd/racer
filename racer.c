#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <unistd.h>

#include "lib.h"
#include "image_buffer.h"
#include "preview.h"

#define SCENE_ITEMS 6

#define RECURSION_LIMIT 30
int recursion_counter = 0;

Primitive scene[] = {
        {SPHERE, {{{ 0.3, 0.13, -0.3 }, 0.12}}, { 1, 1, 1 },      0.05, GLSS},
        {SPHERE, {{{ 0,   0,    -1},    0.5}},  { 0.8, 0.3, 0.3}, 1,    DIFF},
        {SPHERE, {{{ 1,   0,    -1},    0.5}},  { 0.8, 0.6, 0.2}, 0.03, MTHL},
        {SPHERE, {{{ -1, 0, -1 },       0.5}},  { 1, 1, 1},       0.05, GLSS},
        {SPHERE, {{{ 0, -100.5, -1 },   100}},  { 1, 1, 1},       1,    DIFF},
        {SPHERE, {{{ 0, 1.5, -0.5 },    0.7}},  { 2, 2, 2},       1,    LAMP},
};

HitRecord rec = {0, 0, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, 0, NULL};
//------------------------------------------------------------------
typedef struct Cam {Vec lower_left_corner, horizontal, vertical, origin;} Cam;
Cam new_cam() {
        Cam cam;
        cam.horizontal = cvec(4, 0, 0);
        cam.vertical = cvec(0, 2, 0);
        cam.origin = cvec(0,0,0.15);
        cam.lower_left_corner = cvec(-2, -1, -1);
        return cam;
}
Ray get_ray(Cam cam, float u, float v) {
        return cray(cam.origin,
                          vadd(cam.lower_left_corner,
                   vadd(vmul(cam.horizontal, u),
                        vmul(cam.vertical, v))));
}
//------------------------------------------------------------------
void intersect_sphere(Ray ray, Sphere *sphere) {
        rec = new_hitrecord();
        float t_min = 0.001, t_max = 1000;
        Vec oc = vsub(ray.o, sphere->center);
        float a = dot(ray.d, ray.d);
        float b = dot(oc, ray.d);
        float c = dot(oc, oc) - sphere->radius*sphere->radius;
        float discriminant = b*b - a*c;

        if (discriminant>0) {
                float temp = (-b - sqrt(b*b-a*c))/a;
                if (temp < t_max && temp > t_min) {
                        rec.hit = 1;
                        rec.t = temp;
                        rec.direction = direction(ray);
                        rec.p = point_at_parameter (ray, rec.t);
                        rec.normal = vdiv(vsub(rec.p, sphere->center), sphere->radius);
                }
        }

}
void intersection(Ray ray) {
        for (int id = 0; id < SCENE_ITEMS; id++) {
                switch (scene[id].type) {
                        case SPHERE:
                                intersect_sphere(ray, &(scene[id].primitive.sphere));
                        case TRIANGLE:
                                ; //INTERSECT_TRIANGLE
                        default:
                                ;
                }
                if (rec.hit) {
                        rec.material = scene[id].material;
                        rec.object = &scene[id];
                        break;
                }
        }
}

Vec random_in_unit_sphere() {
        Vec p;
        do {
                p = vsub(vmul(cvec(drand48(), drand48(), drand48()), 2.0), cvec(1, 1, 1));
        } while (squared_lenght (p) >= 1.0);
        return p;
}
void fuzz(Ray *r, float albedo) {
        (*r).d = vadd((*r).d, vmul(random_in_unit_sphere(), albedo));
}
Vec skybox(Ray ray) {
        Vec unit_direction = unit_vector(direction(ray));
        float t = 0.5*(unit_direction.y +1.0);
        Vec first_col = cvec(1, 1, 1), second_col = cvec(0.5, 0.7, 0.7);
        return vadd(vmul(first_col, (1.0-t)), vmul(second_col, t));
}
Vec diffuse();
Vec glass();
Vec metal();
Vec lamp();
Vec trace(Ray ray) {
        recursion_counter++;
        if (recursion_counter == RECURSION_LIMIT)
                return cvec(0, 0, 0);

        intersection(ray);
        if (rec.hit) {
                switch (rec.material) {
                        case DIFF:
                                return diffuse();
                        case GLSS:
                                return glass();
                        case MTHL:
                                return metal();
                        case LAMP:
                                return lamp();
                        default:
                                cvec(0, 0, 0);
                }
        }

        return skybox(ray);
}

Vec diffuse() {
        Vec target = vadd(rec.p, vadd(rec.normal, random_in_unit_sphere ()));
        return vmulv(vmul(trace(cray(rec.p, vsub(target, rec.p))), 0.5), (*rec.object).color);
}
Vec glass() {
        Vec outward_normal; Ray scattered;
        Vec uv = unit_vector(rec.direction);
        if (drand48() > 0.9) {
                scattered = cray(rec.p, vsub(uv, vmul(rec.normal, 2*dot(uv, rec.normal))));
                goto ret;
        }
        if (dot(rec.direction, rec.normal) > 0) {
                outward_normal = vmul(rec.normal, -1);
        } else {
                outward_normal = rec.normal;
        }
        float refractive_indices = 1.3;
        float dt = dot(uv, outward_normal);
        float discriminant = 1.0 - pow(refractive_indices,2) * (1-pow(dt,2));
        if (discriminant>0) {
                Vec refracted = vsub(vmul(vsub(uv, vmul(outward_normal, dt)), refractive_indices), 
                                                vmul(outward_normal, sqrtf(discriminant)));
                scattered = cray(rec.p, refracted);
        } else {
                scattered = cray(rec.p, vsub(uv, vmul(rec.normal, 2*dot(uv, rec.normal))));
        }
        ret:
        fuzz(&scattered, (*rec.object).albedo);
        return vmulv(trace(scattered), (*rec.object).color);

}
Vec metal() {
        Vec v = unit_vector(rec.direction);
        Vec reflected = vsub(v, vmul(rec.normal, 2*dot(v, rec.normal)));
        reflected = vadd(reflected, vmul(random_in_unit_sphere(), (*rec.object).albedo));
        if (dot(reflected, rec.normal) > 0)
                return vmulv(vmul(trace(cray(rec.p, reflected)), 0.9), (*rec.object).color);
        else
                return cvec(0, 0, 0);
}
Vec lamp() {
        return (*rec.object).color;
}
//------------------------------------------------------------------
int main() {

        int cx = 1000, cy = 500, ns = 20; // canvas x, y and samples
        printf("%ix%i %ismpls\n", cx, cy, ns);

        Cam cam = new_cam();
        ImageBuffer image = new_image_buffer(cx, cy);
        Preview preview = preview_toggle(&image);

        for (int y = cy-1; y>=0; y--) {
                for (int x = 0; x<cx; x++) {
                        Vec col = cvec(0,0,0);
                        for (int s = 0; s<5; s++) {
                                float v = (float)(y + drand48()) / (float)cy;
                                float u = (float)(x + drand48()) / (float)cx;
                                recursion_counter = 0;
                                col = vadd(col, trace(get_ray(cam, u, v)));
                        }
                        col = vdiv(col, 5);
                        clamp(&col);
                        float gs = (col.x+col.y+col.x)/3+0.2;
                        if (gs>1) gs = 1;
                        insert_pixel_to_buffer(&image, cvec(gs, gs, gs));
                }
                if(y%30 == 0) preview_update(&preview);
        }
        image.current_pixel=0;

        clock_t time = clock();
        
        for (int y = cy-1; y>=0; y--) {
                for (int x = 0; x<cx; x++) {
                        Vec col = cvec(0,0,0);
                        for (int s = 0; s<ns; s++) {
                                float v = (float)(y + drand48()) / (float)cy;
                                float u = (float)(x + drand48()) / (float)cx;
                                recursion_counter = 0;
                                col = vadd(col, trace(get_ray(cam, u, v)));
                        }
                        col = vdiv(col, ns);
                        clamp(&col);
                        insert_pixel_to_buffer(&image, col);
                        
                        printf("\rRendering %1.1f%%", (int)100-((float)y*100/(float)cy));
                }
                if(y%6 == 0) preview_update(&preview);
        }
        export_buffer_to_ppm(&image, "output.ppm");
        
        time = clock() - time;
        printf("\nDone in %f seconds :D It's a lot of time!\n", ((float)time)/CLOCKS_PER_SEC);

        preview_freeze(&preview);

        quit_preview(&preview);
        free_image_buffer(&image);
        return 0;
}

