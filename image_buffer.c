#include "image_buffer.h"


ImageBuffer new_image_buffer(int width, int height) {
    ImageBuffer image_buffer;
    image_buffer.width = width;
    image_buffer.height = height;
    image_buffer.current_pixel = 0;
    image_buffer.buffer = (Vec*) malloc((width*height)*sizeof(Vec));
    //Fill buffer with checked pattern
    int side_lenght = 10;
    for (int y = 0; y<height; y++) {
        for (int x = 0; x<width; x++) {
            int q = (y/side_lenght) % 2 ? 0 : 1;
            image_buffer.buffer[y*width+x] = (x/side_lenght) % 2 == q ? (Vec) {0.8, 0.8, 0.8} : (Vec) {0.6, 0.6, 0.6};
        }
    }
    return image_buffer;
}

void insert_pixel_to_buffer(ImageBuffer* image_buffer, Vec pixel) {
    image_buffer->buffer[image_buffer->current_pixel] = pixel;
    image_buffer->current_pixel++;
}

void export_buffer_to_ppm(ImageBuffer *image_buffer, char* filename) {
    FILE *output = fopen_ppm(filename, image_buffer->width, image_buffer->height);
    for (int i = 0; i<(image_buffer->width*image_buffer->height); i++) {
        insert_pixel(output, image_buffer->buffer[i]);
    }
    fclose(output);
}

void free_image_buffer(ImageBuffer* image_buffer) {
    free(image_buffer->buffer);
}