#include "preview.h"


Preview preview_toggle(ImageBuffer* image_buffer) {
    SDL_Init(SDL_INIT_VIDEO);
    Preview preview;
    preview.image_buffer = image_buffer;
    SDL_CreateWindowAndRenderer(preview.image_buffer->width, 
        preview.image_buffer->height, 
        0, 
        &preview.window, 
        &preview.renderer);
    return preview;
}

void preview_update(Preview* preview) {
    int width = preview->image_buffer->width;
    int height = preview->image_buffer->height;
    for (int y = 1; y<=height; y++) {
        for (int x = 1; x<=width; x++) {
            Rgb rgb = vec_to_rgb(&preview->image_buffer->buffer[y*width-(width-x)-1]);
            SDL_SetRenderDrawColor(preview->renderer, rgb.red, rgb.green, rgb.blue, 255);
            SDL_RenderDrawPoint(preview->renderer, x-1, y-1); 
        }
    }
    SDL_RenderPresent(preview->renderer);
}

void preview_freeze(Preview* preview) {
    SDL_Event e;
    int quit = 0;
    while(!quit) {
        while(SDL_PollEvent(&e)) {
            if(e.type == SDL_QUIT) {
                quit = 1;
            }
        }
    }

}

void quit_preview(Preview* preview) {
    SDL_DestroyRenderer(preview->renderer);
    SDL_DestroyWindow(preview->window);
    SDL_Quit();
}
