BUILD_DIR ?= ./build

SRCS := racer.c preview.c lib.c image_buffer.c
OBJS := ${SRCS:%=$(BUILD_DIR)/%.o}
OBJS := ${OBJS:.c.o=.o}

LIBS := -lm -lSDL2
FLAGS := -Wall -O3

Racer: $(BUILD_DIR) $(OBJS)
	$(CC) $(OBJS) -o $@ $(LIBS)
	./Racer

$(BUILD_DIR):
	mkdir -p $(BUILD_DIR)

$(BUILD_DIR)/%.o: %.c
	$(CC) -c $< -o $@ $(FLAGS)

clean:
	rm -r $(BUILD_DIR)