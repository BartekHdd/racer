#ifndef PREVIEW
#define PREVIEW

#include <SDL2/SDL.h>

#include "lib.h"
#include "image_buffer.h"

typedef struct Preview { 
    SDL_Window* window; 
    SDL_Renderer* renderer; 
    ImageBuffer* image_buffer; 
} Preview;

Preview preview_toggle(ImageBuffer* image_buffer);
void preview_update(Preview* preview);
void preview_freeze(Preview* preview);
void quit_preview(Preview* preview);

#endif //PREVIEW