#include "lib.h"


//VEC///////////////////////////////////////////////////////////////////
Vec cvec(float x, float y, float z)
        { Vec v; v.x = x; v.y = y; v.z = z; return v; }

Vec vadd(Vec v1, Vec v2)
        { v1.x += v2.x; v1.y += v2.y; v1.z += v2.z; return v1; }

Vec vsub(Vec v1, Vec v2)
        { v1.x -= v2.x; v1.y -= v2.y; v1.z -= v2.z; return v1; }

Vec vmul(Vec v, float a)
        { v.x *= a;  v.y *= a;  v.z *= a; return  v; }

Vec vdiv(Vec v, float a)
        { v.x /= a;  v.y /= a;  v.z /= a; return  v; }

Vec vmulv(Vec v1, Vec v2)
        { v1.x *= v2.x; v1.y *= v2.y; v1.z *= v2.z; return v1; }

float dot(Vec v1, Vec v2)
        { return v1.x*v2.x + v1.y*v2.y + v1.z*v2.z; }

float lenght(Vec v)
        { return sqrtf(v.x*v.x + v.y*v.y + v.z*v.z); }

float squared_lenght(Vec v)
        { return v.x*v.x + v.y*v.y + v.z*v.z; }

Vec unit_vector(Vec v)
        { return vdiv(v, lenght(v)); }
//RAY///////////////////////////////////////////////////////////////////

Ray cray(Vec origin, Vec direction)
        { Ray r; r.o   = origin; r.d = direction; return r; }

Vec origin(Ray r)
        { return r.o; }

Vec direction(Ray r)
        { return r.d; }

Vec point_at_parameter(Ray r, float t)
        { return vadd(r.o, vmul(r.d, t)); }

void clamp(Vec *v)  {
        if ((*v).x > 1) (*v).x = 1;
        if ((*v).y > 1) (*v).y = 1; 
        if ((*v).z > 1) (*v).z = 1; 
}
//RGB///////////////////////////////////////////////////////////////////
Rgb vec_to_rgb(Vec* color) {
        Rgb rgb = {(int)(255.99*color->x),  //Red
                   (int)(255.99*color->y),  //Green
                   (int)(255.99*color->z)}; //Blue
        return rgb;
}
//PPM///////////////////////////////////////////////////////////////////
FILE *fopen_ppm(char *name, int width, int height) {
        FILE *ppm = fopen(name, "w");
        fprintf(ppm, "P3 %i %i 255\n", width, height);
        return ppm;
}

void insert_pixel(FILE *file, Vec color) {
        Rgb rgb = vec_to_rgb(&color);
        fprintf(file, "%i %i %i\n", rgb.red, rgb.green, rgb.blue);
}
//PRIMITIVES AND MATERIALS//////////////////////////////////////////////

//HIT///////////////////////////////////////////////////////////////////

HitRecord new_hitrecord()
        { HitRecord rec; rec.hit = 0; rec.t = 0;
        rec.p = cvec(0,0,0); rec.normal = cvec(0,0,0); rec.direction = cvec(0,0,0);
         rec.material = DIFF; rec.object = NULL; return rec; }

