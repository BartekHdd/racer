#ifndef LIB_H
#define LIB_H

#include <stdio.h>
#include <math.h>

//VEC///////////////////////////////////////////////////////////////////
typedef struct Vec {float x, y, z;} Vec;


Vec cvec(float x, float y, float z);

Vec vadd(Vec v1, Vec v2);

Vec vsub(Vec v1, Vec v2);

Vec vmul(Vec v, float a);

Vec vdiv(Vec v, float a);

Vec vmulv(Vec v1, Vec v2);

float dot(Vec v1, Vec v2);

float lenght(Vec v);

float squared_lenght(Vec v);

Vec unit_vector(Vec v);
//RAY///////////////////////////////////////////////////////////////////
typedef struct Ray { Vec o, d; } Ray;

Ray cray(Vec origin, Vec direction);

Vec origin(Ray r);

Vec direction(Ray r);

Vec point_at_parameter(Ray r, float t);
void clamp(Vec *v);
//RGB///////////////////////////////////////////////////////////////////
typedef struct Rgb {int red, green, blue; } Rgb;
Rgb vec_to_rgb(Vec* color);
//PPM///////////////////////////////////////////////////////////////////
FILE *fopen_ppm(char *name, int width, int height);

void insert_pixel(FILE *file, Vec color);
//PRIMITIVES AND MATERIALS//////////////////////////////////////////////
typedef enum { DIFF, GLSS, MTHL, LAMP} Material;

typedef enum { SPHERE, TRIANGLE } PrimitiveType;
typedef struct Sphere { Vec center; float radius; } Sphere;
typedef struct Triangle { Vec x, y; } Triangle;

typedef struct Primitive { 
        PrimitiveType type; 
        union { 
                Sphere sphere; 
                Triangle triangle; 
        } primitive;
        Vec color;
        float albedo;
        Material material;
} Primitive;
//HIT///////////////////////////////////////////////////////////////////
typedef struct HitRecord {
        int hit; 
        float t; 
        Vec p, normal, direction; 
        Material material; 
        Primitive* object; 
} HitRecord;

HitRecord new_hitrecord();
#endif //LIB_H
