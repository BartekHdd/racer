#ifndef IMAGE_BUFFER
#define IMAGE_BUFFER

#include <stdlib.h>

#include "lib.h"

typedef struct ImageBuffer {
    int width, height, current_pixel;
    Vec* buffer;
} ImageBuffer;

ImageBuffer new_image_buffer(int width, int height);

void insert_pixel_to_buffer(ImageBuffer* image_buffer, Vec pixel);
void export_buffer_to_ppm(ImageBuffer *image_buffer, char* filename);
void free_image_buffer(ImageBuffer* image_buffer);

#endif //IMAGE_BUFFER